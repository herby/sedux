Smalltalk createPackage: 'Sedux'!
Object subclass: #SeduxCreatorClass
	instanceVariableNames: ''
	package: 'Sedux'!

!SeduxCreatorClass class methodsFor: 'as yet unclassified'!

<< aStoreEnhancer
	^ aStoreEnhancer next: self
!

inject: anObject
	^ self inject: anObject into: nil
! !

SeduxCreatorClass subclass: #Sedux
	instanceVariableNames: ''
	package: 'Sedux'!

!Sedux class methodsFor: 'as yet unclassified'!

dispatch: anAction with: aReceiver fallback: aBlock
	^ (aReceiver respondsTo: anAction selector)
		ifTrue: [ anAction sendTo: aReceiver ]
		ifFalse: aBlock
!

inject: aReducer into: anObject
	"Creates a store, dispatches initiating message"
! !

SeduxCreatorClass subclass: #SeduxDecorator
	instanceVariableNames: 'next'
	package: 'Sedux'!

!SeduxDecorator methodsFor: 'accessing'!

next: anObject
	next := anObject
! !

SeduxDecorator class instanceVariableNames: 'next'!

!SeduxDecorator class methodsFor: 'as yet unclassified'!

inject: aReducer into: anObject
	^ SeduxDecoratedStore dispatch: self new next: (self nextInject: aReducer into: anObject)
!

next: aStoreCreator
	next := aStoreCreator.
	^self
!

nextInject: aReducer into: anObject
	^ next inject: aReducer into: anObject
! !

SeduxDecorator subclass: #SeduxFoo
	instanceVariableNames: ''
	package: 'Sedux'!

Object subclass: #SeduxDecoratedStore
	instanceVariableNames: 'next dispatch'
	package: 'Sedux'!

!SeduxDecoratedStore methodsFor: 'accessing'!

dispatch: anAction
	^ Sedux
		dispatch: anAction
		with: dispatch
		fallback: [ ^ next dispatch: anAction ]
!

dispatch: aDispatcher next: aStore
	dispatch := aDispatcher.
	next := aStore
! !

!SeduxDecoratedStore class methodsFor: 'instance creation'!

dispatch: aDispatcher next: aStore
	^ self new
		dispatch: aDispatcher next: aStore;
		yourself
! !

