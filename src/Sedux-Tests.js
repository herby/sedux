define("sedux/Sedux-Tests", ["amber/boot", "amber_core/SUnit"], function($boot){"use strict";
var $core=$boot.api,nil=$boot.nil,$recv=$boot.asReceiver,$globals=$boot.globals;
$core.addPackage('Sedux-Tests');
$core.packages["Sedux-Tests"].innerEval = function (expr) { return eval(expr); };
$core.packages["Sedux-Tests"].transport = {"type":"amd","amdNamespace":"sedux"};

$core.addClass('SeduxTest', $globals.TestCase, [], 'Sedux-Tests');

});
