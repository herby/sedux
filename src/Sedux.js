define("sedux/Sedux", ["amber/boot", "amber_core/Kernel-Objects"], function($boot){"use strict";
var $core=$boot.api,nil=$boot.nil,$recv=$boot.asReceiver,$globals=$boot.globals;
$core.addPackage('Sedux');
$core.packages["Sedux"].innerEval = function (expr) { return eval(expr); };
$core.packages["Sedux"].transport = {"type":"amd","amdNamespace":"sedux"};

$core.addClass('SeduxCreatorClass', $globals.Object, [], 'Sedux');

$core.addMethod(
$core.method({
selector: "<<",
protocol: 'as yet unclassified',
fn: function (aStoreEnhancer){
var self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aStoreEnhancer)._next_(self);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"<<",{aStoreEnhancer:aStoreEnhancer},$globals.SeduxCreatorClass.klass)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aStoreEnhancer"],
source: "<< aStoreEnhancer\x0a\x09^ aStoreEnhancer next: self",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["next:"]
}),
$globals.SeduxCreatorClass.klass);

$core.addMethod(
$core.method({
selector: "inject:",
protocol: 'as yet unclassified',
fn: function (anObject){
var self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return self._inject_into_(anObject,nil);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inject:",{anObject:anObject},$globals.SeduxCreatorClass.klass)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "inject: anObject\x0a\x09^ self inject: anObject into: nil",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["inject:into:"]
}),
$globals.SeduxCreatorClass.klass);


$core.addClass('Sedux', $globals.SeduxCreatorClass, [], 'Sedux');

$core.addMethod(
$core.method({
selector: "dispatch:with:fallback:",
protocol: 'as yet unclassified',
fn: function (anAction,aReceiver,aBlock){
var self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=$recv(aReceiver)._respondsTo_($recv(anAction)._selector());
return $recv($1)._ifTrue_ifFalse_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(anAction)._sendTo_(aReceiver);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"dispatch:with:fallback:",{anAction:anAction,aReceiver:aReceiver,aBlock:aBlock},$globals.Sedux.klass)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAction", "aReceiver", "aBlock"],
source: "dispatch: anAction with: aReceiver fallback: aBlock\x0a\x09^ (aReceiver respondsTo: anAction selector)\x0a\x09\x09ifTrue: [ anAction sendTo: aReceiver ]\x0a\x09\x09ifFalse: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["ifTrue:ifFalse:", "respondsTo:", "selector", "sendTo:"]
}),
$globals.Sedux.klass);

$core.addMethod(
$core.method({
selector: "inject:into:",
protocol: 'as yet unclassified',
fn: function (aReducer,anObject){
var self=this;
return self;

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aReducer", "anObject"],
source: "inject: aReducer into: anObject\x0a\x09\x22Creates a store, dispatches initiating message\x22",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.Sedux.klass);


$core.addClass('SeduxDecorator', $globals.SeduxCreatorClass, ['next'], 'Sedux');
$core.addMethod(
$core.method({
selector: "next:",
protocol: 'accessing',
fn: function (anObject){
var self=this;
self["@next"]=anObject;
return self;

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "next: anObject\x0a\x09next := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.SeduxDecorator);


$globals.SeduxDecorator.klass.iVarNames = ['next'];
$core.addMethod(
$core.method({
selector: "inject:into:",
protocol: 'as yet unclassified',
fn: function (aReducer,anObject){
var self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.SeduxDecoratedStore)._dispatch_next_(self._new(),self._nextInject_into_(aReducer,anObject));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"inject:into:",{aReducer:aReducer,anObject:anObject},$globals.SeduxDecorator.klass)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aReducer", "anObject"],
source: "inject: aReducer into: anObject\x0a\x09^ SeduxDecoratedStore dispatch: self new next: (self nextInject: aReducer into: anObject)",
referencedClasses: ["SeduxDecoratedStore"],
//>>excludeEnd("ide");
messageSends: ["dispatch:next:", "new", "nextInject:into:"]
}),
$globals.SeduxDecorator.klass);

$core.addMethod(
$core.method({
selector: "next:",
protocol: 'as yet unclassified',
fn: function (aStoreCreator){
var self=this;
self["@next"]=aStoreCreator;
return self;

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aStoreCreator"],
source: "next: aStoreCreator\x0a\x09next := aStoreCreator.\x0a\x09^self",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.SeduxDecorator.klass);

$core.addMethod(
$core.method({
selector: "nextInject:into:",
protocol: 'as yet unclassified',
fn: function (aReducer,anObject){
var self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(self["@next"])._inject_into_(aReducer,anObject);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"nextInject:into:",{aReducer:aReducer,anObject:anObject},$globals.SeduxDecorator.klass)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aReducer", "anObject"],
source: "nextInject: aReducer into: anObject\x0a\x09^ next inject: aReducer into: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["inject:into:"]
}),
$globals.SeduxDecorator.klass);


$core.addClass('SeduxFoo', $globals.SeduxDecorator, [], 'Sedux');


$core.addClass('SeduxDecoratedStore', $globals.Object, ['next', 'dispatch'], 'Sedux');
$core.addMethod(
$core.method({
selector: "dispatch:",
protocol: 'accessing',
fn: function (anAction){
var self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
return $recv($globals.Sedux)._dispatch_with_fallback_(anAction,self["@dispatch"],(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
throw $early=[$recv(self["@next"])._dispatch_(anAction)];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}));
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"dispatch:",{anAction:anAction},$globals.SeduxDecoratedStore)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAction"],
source: "dispatch: anAction\x0a\x09^ Sedux\x0a\x09\x09dispatch: anAction\x0a\x09\x09with: dispatch\x0a\x09\x09fallback: [ ^ next dispatch: anAction ]",
referencedClasses: ["Sedux"],
//>>excludeEnd("ide");
messageSends: ["dispatch:with:fallback:", "dispatch:"]
}),
$globals.SeduxDecoratedStore);

$core.addMethod(
$core.method({
selector: "dispatch:next:",
protocol: 'accessing',
fn: function (aDispatcher,aStore){
var self=this;
self["@dispatch"]=aDispatcher;
self["@next"]=aStore;
return self;

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aDispatcher", "aStore"],
source: "dispatch: aDispatcher next: aStore\x0a\x09dispatch := aDispatcher.\x0a\x09next := aStore",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.SeduxDecoratedStore);


$core.addMethod(
$core.method({
selector: "dispatch:next:",
protocol: 'instance creation',
fn: function (aDispatcher,aStore){
var self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
$1=self._new();
$recv($1)._dispatch_next_(aDispatcher,aStore);
return $recv($1)._yourself();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"dispatch:next:",{aDispatcher:aDispatcher,aStore:aStore},$globals.SeduxDecoratedStore.klass)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aDispatcher", "aStore"],
source: "dispatch: aDispatcher next: aStore\x0a\x09^ self new\x0a\x09\x09dispatch: aDispatcher next: aStore;\x0a\x09\x09yourself",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["dispatch:next:", "new", "yourself"]
}),
$globals.SeduxDecoratedStore.klass);

});
