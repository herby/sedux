define([
    'amber/deploy',
    // --- packages to be deployed begin here ---
    'sedux/Sedux'
    // --- packages to be deployed end here ---
], function (amber) {
    return amber;
});
